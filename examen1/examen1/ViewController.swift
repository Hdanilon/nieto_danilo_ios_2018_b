//
//  ViewController.swift
//  examen1
//
//  Created by Danilo Nieto on 27/11/18.
//  Copyright © 2018 Danilo Nieto. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var useranameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    @IBAction func buttonEntrar(_ sender: Any) {
        let username = useranameTextField.text!
        let password = passwordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            if let error = error{
                print(error)
                return
            }
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
    }
    
}

