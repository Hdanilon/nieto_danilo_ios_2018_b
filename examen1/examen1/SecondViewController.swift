//
//  SecondViewController.swift
//  examen1
//
//  Created by Danilo Nieto on 28/11/18.
//  Copyright © 2018 Danilo Nieto. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var res: UILabel!
    let num = NumPares()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func obtener(_ sender: Any) {
        let numero = name.text!
        let entero = Int(numero)
        
        num.obtenerNumeros(entero ?? 0)
        
        let stringNumero = String (num.serie)
        
        res.text = stringNumero
    }
    
    @IBAction func salir(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
