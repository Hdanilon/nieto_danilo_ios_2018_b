//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var n=5
// if

if n > 2{
    print("it's greater")
}else{
    print("it's lower")
}

for _ in 1...10{
    print("hola")
}

for i in 1...10 {
    print("Number is: \(i)")
}

for i in 1...20{
    if i % 2 == 0{
        print(i)
    }
}

for i in 1...20 where i % 2 == 0{
    print(i)
}


var j = 10

while j>0{
    print(j)
    j-=1
}

repeat{
    print(j)
    j += 1
}while j < 10

let i = 10
switch i {
case 1:
    print("something")
case 2:
    print("2")
default:
    print("something else")
}

// a = edad de una persona
// switch para imprimir si la persona es mayor, menor, 3ra
//comentar: shift + command +7

let a = 20

switch a {
case 0...17:
    print("Ilegal")
case 18...65:
    print("Adulto")
default:
    print("elder")
}
//ver el tipo de variable option + click
var numbers = Array(1...10)
let evenNumbers = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
let emptyIntArray:[Int] = []

numbers.append(11)
numbers += [12, 13]

numbers.remove(at: 3)//indice

//mayores a 16
var filterdNumbers = numbers.filter { (n) -> Bool in n > 10}

filterdNumbers = numbers.filter{ $0 > 10 }

var squareNumbers = numbers.map { (n) in return n * n }

squareNumbers = numbers.map { $0 * $0 }

var sum = numbers.reduce(0) { r, n in r + n}

sum = numbers.reduce(0) { $0 + $1 }
//sort arregla el arreglo
//sorted genera un nuevo arreglo ordenado
numbers.sort { (a, b) -> Bool in a < b }

numbers.sort { $0 < $1 }

var dict:[String:Any] = [
    "Name": "Danilo",
    "LastName": "Nieto",
    "age": 25
]

dict["Name"]

dict["country"] = "Ecuador"

//tupla

//let tupla = (1, 2, "Hola")
////acceder por el indice
//tupla.0
//tupla.1
//tupla.2
//
//let otra = (x:1, y:2, z:45.2, name: "Danilo")
//
//otra.z
//otra.name
//otra.x
//
//let (x, y, z, name) = (1, 2, 3, "Danilo")
//
//x
//y
//z
//name


// functions

func functionName(){
    //do something cool
}

func intFunction() -> Int {
    return 1
}

func sum(n1:Int, n2:Int, n3:Int = 0 ) -> Int {
    return n1 + n2 + n3
}

sum(n1:1, n2:2, n3:3)
sum(n1:3, n2:4)

func sumPro(_ n1:Int,_ n2:Int,_ n3:Int) -> Int {
    return n1 + n2 + n3
}

sumPro(1, 2, 3)

var result = 0
func changeResult(n: inout Int) {
    result = 20
}
changeResult(n: &result)

result

func calc(_ n1:Int,_ n2:Int) -> (Int, Int, Int) {
    return (n1 + n2, n1 - n2, n1 * n2)
}

let (add, td, times) = calc(5, 2)

add
td
times

func poww(a:Int, b:Int) -> Int{
    if b == 1 {
        return a
    }else {
        return a * poww (a: a, b: b - 1)
    }
}

poww(a: 2, b: 3)

//Struct & Class

struct Teacher {
    var name: String?
    var lastName: String?
    
    
    func something(){
        // do something
    }
}//referencia por valor

class Person {
    var ci:Int?//la ? es para valor opcional
    
    func printSomething(){
        print("Something")
    }
}

class Student: Person {
    var name: String?
    var lastName: String?
    
    func something(){
        //do something
    }
    
    override func printSomething() {
        print("other thing")
    }
}//referencia por memoria

var t1 = Teacher()
t1.name = "Pepito"
t1.lastName = "Patito"

var t2 = t1
t2.name = "Nuevo Pepito"

t1.name//Pepito
t2.name//Nuevo Pepito

var s1 = Student()
s1.name = "Pepito"
s1.lastName = "Patito"
s1.ci = 17543768
var s2 = s1
s2.name = "Nuevo Pepito"

s1.name//Nuevo Pepito
s2.name//Nuevo Pepito

var optional:Int? = 4
var nonOptional:Int = 8

optional = 19
nonOptional = 16

print(optional)//Optional(19)\n
print(optional ?? 0)//usar el valor del optional y si no hay usa 0
print(nonOptional)//16\n

enum MyError: Error{
    case runtimeError(String)
}

func canThrow(n:Int) throws -> Int{
    if n == 0 {
        throw MyError.runtimeError("Error")
    }
    return n * n
}


do{
    try canThrow(n: 0)
}catch{
    print("$%& happens")
}































